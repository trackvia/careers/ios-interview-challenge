import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Welcome to the TrackVia iOS Challenge")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
