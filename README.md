# TrackVia iOS Challenges
Welcome to the TrackVia iOS Candidate Challenges! These challenges are designed to give you an opportunity to show us your skills and simulate real-world tasks you might encounter while working with our team.

## Challenges
Pleaese select one (or both!) of the following challenges:
- [Value Management](./iOS Challenges/Sources/ValueManagement/)
- [Connect Four](./iOS Challenges/Sources/ConnectFour/)

## Requirements
- Use Xcode 14 or the Swift 5.7 toolchain. Please do not use any beta software to complete the challenge.
- Please complete the challenge within 7 days of receipt
- Include any non-standard instructions needed to build or run the code in a README 

## Considerations
- This is your chance to flex your development skills
- Think about writing reliable, maintainable code
- Add comments to help explain your thinking or TODOs for future improvements
- More code is not always better!

## Deliverables
Open a merge request to this repository with your implementation. In the merge request description, please note any tradeoffs you made, considerations for future improvements or explanations for why you made the decisions you did.
