import Foundation
import ValueManagement

let firstNameField = Form.TextField("", rules: [.isNotEmpty])
let lastNameField = Form.TextField("", rules: [.isNotEmpty])
let ageField = Form.NumberField(nil, rules: [.contained(in: 18..<150)])

let form = Form(fields: [firstNameField, lastNameField, ageField])

form.validate()
