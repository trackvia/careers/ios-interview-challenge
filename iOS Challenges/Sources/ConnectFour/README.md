# Developer Challenge: Connect Four

## Overview:
You are tasked with building a basic implementation of the popular children’s game “Connect Four”. Feel free to use any libraries you consider appropriate.

## Deliverables:
Please open a merge request with your implementation

## Requirements:
1. Game Grid Size is 7 columns by 6 rows (but consider different sizes, too)
1. The game starts by asking Player 1 to select a color (black or red). Player 2 is automatically assigned the other color. 
1. Players take alternating turns placing tiles until an outcome is achieved (black player win, red player win or draw). 
1. When a game outcome is reached, the result should be displayed to the user in an alert 

## Other Considerations:
- Optimization of the win check
- Test coverage of the key algorithm(s)
- Thoughtful UI styling / interaction is nice to have, but not required
- Anything else that might help showcase your skills as a developer
- Be sure to consider value vs effort in regards to extra credit. More code isn’t always better.

## Wireframe:
![Wireframe](./wireframe.png)