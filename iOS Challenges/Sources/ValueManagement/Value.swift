import Foundation

/// - Tag: Valuable
public protocol Valuable {
    associatedtype ValueType
    var value: ValueType { get }
    mutating func update(_ newValue: ValueType) throws
}

public class Value<T>: Valuable, Validatable {
    public typealias ValidationType = T
    
    public private(set) var value: T
    public let rules: [any Rulable<T>]
    
    public init(_ initialValue: T, rules: [Rule<T>] = []) {
        self.value = initialValue
        self.rules = rules
    }
    
    public convenience init(_ initialValue: T, rules: Rule<T>...) {
        self.init(initialValue, rules: rules)
    }
    
    public func update(_ newValue: T) throws {
        self.value = newValue
    }
}
