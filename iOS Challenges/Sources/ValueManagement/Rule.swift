import Foundation

/// - Tag: Rulable
public protocol Rulable<RuleType> {
    associatedtype RuleType
    func evaluate(_ value: RuleType) throws
}

public struct Rule<T>: Rulable {
    public typealias EvaluationHandler = (T) throws -> Void
    private let evaluationHandler: EvaluationHandler
    
    public init(_ evaluationHandler: @escaping EvaluationHandler = { _ in }) {
        self.evaluationHandler = evaluationHandler
    }
    
    public func evaluate(_ value: T) throws {
        try evaluationHandler(value)
    }
}

extension Rule where T: AnyOptional {
    public static var `required`: Self {
        .init { value in
            guard value.value != nil else { throw Invalidation.missingRequiredValue }
        }
    }
    
    public static func contained(in range: Range<T.WrappedType>) -> Self where T.WrappedType: Comparable {
            return .init {
                guard let wrappedValue = $0.value else { throw Invalidation.missingRequiredValue }
                guard range.contains(wrappedValue) else { throw Invalidation.outOfBounds }
            }
        }

}

extension Rule where T: Collection {
    public static var isNotEmpty: Self {
        .init { collection in
            guard !collection.isEmpty else { throw Invalidation.isEmpty }
        }
    }
}
