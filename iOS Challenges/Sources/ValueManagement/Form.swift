import Foundation

public final class Form: Validatable {
    public var rules: [any Rulable<FieldType>] = []
    let fields: [any FieldType]

    public init(fields: [any FieldType]) {
        self.fields = fields
    }
    
    public func validate() -> [Invalidation] {
        return fields.flatMap { $0.validate() }
    }
}

public extension Form {
    typealias FieldType = ValidatableValue
    typealias TextField = Field<String>
    typealias NumberField = Field<Double?>
    
    class Field<T>: Value<T> { }
}
