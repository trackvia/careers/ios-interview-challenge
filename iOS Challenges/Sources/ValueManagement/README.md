# Value Management
This challenge focuses on data modeling, managing and validating values, working in an existing codebase, and advanced Swift language features such as generics.


## Prompt
Think about a digital form. A form is typically made of sections containing fields. Each field has its own context or type (string, number, date, date/time, location, a set of options, an image or file, etc). To submit the form, all of its contained fields need to be valid. In order for a field to be valid, its underlying value needs to be validated against a set of rules that are associated to that field such as:

- The field is required (must have a value)
- Minimum/Maximum value
- Character set inclusion/exclusion input constraints
- Value falls inside/outside a given range
- A minimum/maximum number of selections were made from a set of options

Your solution should be able to support any type of form, ranging from a simple sign-up form with username and password fields to a complex intake form with multiple sections and an arbitrary number of fields of varying types. Checkout the provided Swift Playground in the repository's root directory for an example of a simple form that can be built using the existing API.


## The Challenge
**Given the [`Valuable`](x-source-tag://Valuable), [`Validatable`](x-source-tag://Validatable), and [`Rulable`](x-source-tag://Rulable) protocols and their concrete implementations, expand on the existing form/field/validation/rules data model to create the following contact form.**


| Field  | Rules  |
| ------ | ------ |
| Name  | required |
| Email | must be a valid email |
| Age   | must be 18 or older |
| Subject | must be one of the following: general, suggestion, help |
| Message | required, must be under 500 characters long |f
| Stay in touch | must be true or false |


- Your solution must be entirely implemented in the `ValueManagement` target of the `iOS Challenges` Swift package.
- Try to use the existing code as it's provided. If you must change it or feel like there is a better way to redefine the existing APIs, be prepared to explain why your changes were necessary.
- Provide unit tests to show your solution in action. You are not expected to provide unit tests for the existing API, only for code that you provide.
- If there are more rules that can be implemented and added to the above fields then please feel free to do so.
- Also feel free to build out more complex forms in the playground as well but this isn't required. Sometimes it's nice to see how APIs are used, how ergonomic they are or are not, and how they read without the noise or overhead of reading unit tests.
- Focus on the data model rather than UI. If you're satisfied with your updates and their tests then feel free to build a UI, but it is not a requirement for this challenge. 
- If you'd like to build even more complex forms please feel free to do so!
- Leave code comments where ever appropriate. 
- Include links to any online resources that you might have used during your implementation.
- The use of 3rd party / open source solutions is *not* allowed for this challenge.


## Want to see
- Strong understanding of Swift generics, protocol oriented programming, and extensibility
- DRY & SOLID principles on display
- Code readability, organization and API design
- Correctness of new code confirmed by unit tests
- How you work within an existing code


## Don't want to see
- Completeness. This doesn't need to be production ready code! Don't sweat it if you don't support every field type and it's validations. 
- UIKit/SwiftUI code
- Someone else's code!


## Discussion
- What alternatives do we have to the use of generics in this challenge? 
- What are the pros/cons of using generics here versus the pros/cons of any alternatives?
- Please call out any other points worth discussing in code comments or comments on the merge request for your submission

