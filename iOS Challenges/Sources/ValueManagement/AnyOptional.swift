import Foundation

public protocol AnyOptional: ExpressibleByNilLiteral {
    associatedtype WrappedType
    var value: WrappedType? { get }
}

extension Optional: AnyOptional {
    public var value: Wrapped? { self.flatMap { $0 } }
}

