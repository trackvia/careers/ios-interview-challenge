import Foundation

public typealias ValidatableValue = Valuable & Validatable

public enum Invalidation: Error {
    case missingRequiredValue
    case isEmpty
    case outOfBounds
}

/// - Tag: Validatable
public protocol Validatable<ValidationType> {
    associatedtype ValidationType
    var rules: [any Rulable<ValidationType>] { get }
    func validate() -> [Invalidation]
}

extension Validatable where Self: Valuable, ValidationType == ValueType {
    public func validate() -> [Invalidation] {
        return (try? rules.compactMap { rule -> Invalidation? in
            do {
                try rule.evaluate(value)
                return nil
            } catch let error as Invalidation {
                return error
            }
        }) ?? []
    }
}
