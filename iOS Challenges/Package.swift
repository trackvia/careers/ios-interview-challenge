// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "iOS Challenges",
    platforms: [
        .iOS(.v16)
    ],
    products: [
        .library(
            name: "ValueManagement",
            targets: ["ValueManagement"]),
        .library(
            name: "ConnectFour",
            targets: ["ConnectFour"])
    ],
    dependencies: [],
    targets: [
        .target(
            name: "ValueManagement",
            dependencies: []),
        .testTarget(
            name: "ValueManagementTests",
            dependencies: ["ValueManagement"]),
        .target(
            name: "ConnectFour",
            dependencies: []),
        .testTarget(
            name: "ConnectFourTests",
            dependencies: ["ConnectFour"]),
    ]
)
